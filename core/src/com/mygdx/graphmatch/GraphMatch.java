package com.mygdx.graphmatch;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.graphmatch.screens.GameplayScreen;

/**
 * The main game class that contains single instances to be used
 * by all the screens in the game
 */

public class GraphMatch extends Game
{
	public static final int WORLD_WIDTH = 200;
	public static final int WORLD_HEIGHT = 200;

	//The game's sprite batch
	public static SpriteBatch spriteBatch;

	//The game's shape renderer
	public static ShapeRenderer shapeRenderer;

	//Screens
	private GameplayScreen gameplayScreen;
	
	@Override
	public void create ()
	{
		//Instantiate the renderers
		spriteBatch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();

		//Instantiate and set the screen
		gameplayScreen = new GameplayScreen();
		setScreen(gameplayScreen);
	}

	@Override
	public void render ()
	{
		//Clear the screen and set the background to white
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		super.render();
	}
}
