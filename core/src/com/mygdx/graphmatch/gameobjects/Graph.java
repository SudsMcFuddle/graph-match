package com.mygdx.graphmatch.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.graphmatch.screens.GameplayScreen;

import java.util.ArrayList;

/**
 * Created by Oliver on 7/21/2015.
 *
 * This class holds the shapes used to represent the graph,
 * and methods to draw them on the screen.
 */
public class Graph implements IDrawShape
{
    //The screen that the graph will be drawn on
    GameplayScreen screen;

    //The outline of the graph
    Rectangle graphSquare;
    float x, y, width, height;

    //Size of the graph, based on number of squares in each row/column
    int size;

    //Lines of the graph
    Line[] horizontalLines;
    Line[] verticalLines;

    //Color of the outline and the lines
    private Color color;

    ArrayList<Function> playerFunctions;
    ArrayList<Function> compFunctions;

    //The graph's state machine
    DefaultStateMachine<Graph> stateMachine;

    public Graph(GameplayScreen scr, Color c, int s, float x, float y, float w, float h)
    {
        screen = scr;

        //Instantiates a new rectangle using the given parameters
        graphSquare = new Rectangle(x, y, 0, 0);
        this.x = x; this.y = y; width = w; height = h;

        //Instantiate state machine
        stateMachine = new DefaultStateMachine<Graph>(this, GraphState.DRAW_OUTLINE);

        //Set the graph size
        size = s;

        //Instantiate line arrays
        horizontalLines = new Line[size - 1];
        verticalLines = new Line[size - 1];

        //The initial x and y positions for the first lines in the array
        float lineX = x + width/(horizontalLines.length + 1);
        float lineY = y + height/(horizontalLines.length + 1);

        //Because both arrays are the same size we only need one for loop
        for(int i = 0; i < horizontalLines.length; i++)
        {
            //Instantiate each line from both array
            horizontalLines[i] = new Line(x, lineY, x, lineY);
            verticalLines[i] = new Line(lineX, y, lineX, y);

            //Increment the x and y positions
            lineX += width/(horizontalLines.length + 1);
            lineY += height/(horizontalLines.length + 1);
        }

        //Sets the color
        color = c;

        playerFunctions = new ArrayList<Function>();
        compFunctions = new ArrayList<Function>();
    }

    public GraphState getState()
    {
        return (GraphState)stateMachine.getCurrentState();
    }

    public void setState(GraphState state)
    {
        stateMachine.changeState(state);
    }

    public void addFunction(Function f)
    {
        if(f.getFunctionType() == Function.FunctionType.PLAYER_CONTROLLED) playerFunctions.add(f);
        else if(f.getFunctionType() == Function.FunctionType.COMP_CONTROLLED) compFunctions.add(f);
    }

    public Function getFunction(int loc, Function.FunctionType type)
    {
        return type == Function.FunctionType.PLAYER_CONTROLLED ? playerFunctions.get(loc) : compFunctions.get(loc);
    }

    //Transition method for gradually drawing the graph on the screen
    void drawSquareOnScreen()
    {
        graphSquare.width = MathUtils.lerp(graphSquare.width, width, Gdx.graphics.getDeltaTime() * 5);
        graphSquare.height = MathUtils.lerp(graphSquare.height, height, Gdx.graphics.getDeltaTime() * 5);
    }

    //Transition method for gradually drawing the graph's lines
    void drawLinesOnScreen()
    {
        for(Line l : horizontalLines)
        {
            l.setEndX(MathUtils.lerp(l.getEndX(), graphSquare.x + graphSquare.width, Gdx.graphics.getDeltaTime() * 10));
        }
        for(Line l : verticalLines)
        {
            l.setEndY(MathUtils.lerp(l.getEndY(), graphSquare.y + graphSquare.height, Gdx.graphics.getDeltaTime() * 10));
        }
    }

    //Set all of the lines' x positions to align with the graph
    public void updatePosition()
    {
        //This is similar to instantiating the arrays in the constructor, but we're only messing with x positions
        float lineX = graphSquare.x + width/(horizontalLines.length + 1);

        for(int i = 0; i < horizontalLines.length; i++)
        {
            horizontalLines[i].setStartX(graphSquare.x);
            horizontalLines[i].setEndX(graphSquare.x + graphSquare.width);
            verticalLines[i].setStartX(lineX);
            verticalLines[i].setEndX(lineX);

            lineX += width/(horizontalLines.length + 1);
        }

        for(Function f : playerFunctions)
        {
            f.updatePosition();
        }
        for(Function f : compFunctions)
        {
            f.updatePosition();
        }
    }

    @Override
    public void draw(ShapeRenderer shape)
    {
        stateMachine.update();

        shape.begin(ShapeRenderer.ShapeType.Line);

        Gdx.gl.glLineWidth(1);

        //Set the ShapeRenderer's color
        shape.setColor(color);

        //Draw the graph's outline
        shape.rect(graphSquare.x, graphSquare.y, graphSquare.width, graphSquare.height);

        //Draw each line
        for(Line l : horizontalLines)
        {
            shape.line(l.getStartX(), l.getStartY(), l.getEndX(), l.getEndY());
        }
        for(Line l : verticalLines)
        {
            shape.line(l.getStartX(), l.getStartY(), l.getEndX(), l.getEndY());
        }

        shape.end();

        for(Function f : playerFunctions)
        {
            f.draw(shape);
        }

        for(Function f : compFunctions)
        {
            f.draw(shape);
        }
    }

    @Override
    public void setColor(Color c)
    {
        color = c;
    }

    @Override
    public void setLineThickness(int thickness)
    {
        Gdx.gl20.glLineWidth(thickness);
    }
}
