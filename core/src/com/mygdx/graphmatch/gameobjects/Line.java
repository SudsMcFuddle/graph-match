package com.mygdx.graphmatch.gameobjects;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Oliver on 7/21/2015.
 *
 * A line has 2 points, the start and end, represented by Vector2s
 */
public class Line
{
    //The start and end points
    private Vector2 start;
    private Vector2 end;

    public Line(float x1, float y1, float x2, float y2)
    {
        //Instantiates the points.
        start = new Vector2(x1, y1);
        end = new Vector2(x2, y2);
    }

    //Getters and setters for the start and end points
    public Vector2 getStartPoint() {return start;}
    public float getStartX() {return start.x;}
    public float getStartY() {return start.y;}
    public void setStartPoint(float x, float y) {start.x = x; start.y = y;}
    public void setStartX(float x) {start.x = x;}
    public void setStartY(float y) {start.y = y;}

    public Vector2 getEndPoint() {return end;}
    public float getEndX() {return end.x;}
    public float getEndY() {return end.y;}
    public void setEndPoint(float x, float y) {end.x = x; end.y = y;}
    public void setEndX(float x) {end.x = x;}
    public void setEndY(float y) {end.y = y;}
}
