package com.mygdx.graphmatch.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.graphmatch.GraphMatch;
import com.mygdx.graphmatch.gameobjects.Graph;

import java.util.Random;

/**
 * Stores states used by the Graph's state machine
 */
public enum GraphState implements State<Graph>
{
    //Draw the graph's outer square
    DRAW_OUTLINE()
            {
                @Override
                public void update(Graph graph)
                {
                    graph.drawSquareOnScreen();
                    if(MathUtils.ceil(graph.graphSquare.width) == graph.width)
                    {
                        graph.graphSquare.setWidth(MathUtils.ceil(graph.graphSquare.width));
                        graph.stateMachine.changeState(DRAW_LINES);
                    }
                }
            },

    //Draw the graph's lines
    DRAW_LINES()
            {
                @Override
                public void update(Graph graph)
                {
                    graph.drawLinesOnScreen();
                    if(MathUtils.ceil(graph.horizontalLines[0].getEndX()) == graph.x + graph.width &&
                       MathUtils.ceil(graph.verticalLines[0].getEndY()) == graph.y + graph.height)
                    {
                        graph.stateMachine.changeState(DRAW_FUNCTIONS);
                    }

                }
            },

    //Draw the functions onto the graph
    DRAW_FUNCTIONS()
            {
                @Override
                public void update(Graph graph)
                {
                    //Add lines to each function
                    for(Function f : graph.playerFunctions)
                    {
                        f.addLine();
                    }
                    for(Function f : graph.compFunctions)
                    {
                        f.addLine();
                    }
                    if(graph.getFunction(graph.compFunctions.size()-1, Function.FunctionType.COMP_CONTROLLED).drawLimit >= graph.size * Function.DETAIL) graph.stateMachine.changeState(IDLE);
                }
            },

    //Idle state
    IDLE()
            {
                @Override
                public void update(Graph graph)
                {

                }
            },

    //Gradually move the graph off the screen
    MOVE_OFF_SCREEN()
            {
                float speed;

                @Override
                public void enter(Graph graph)
                {
                    speed = 0.0f;
                }

                @Override
                public void update(Graph graph)
                {
                    graph.graphSquare.setX(graph.graphSquare.x + speed);
                    graph.updatePosition();
                    speed += Gdx.graphics.getDeltaTime() * 10;

                    if(graph.graphSquare.x > Gdx.graphics.getWidth())
                    {
                        graph.stateMachine.changeState(MOVE_ON_SCREEN);
                    }
                }
            },

    //Gradually move the graph onto the screen
    MOVE_ON_SCREEN()
            {
                @Override
                public void enter(Graph graph)
                {
                    graph.graphSquare.setX(-graph.graphSquare.width);

                    for(Function f : graph.playerFunctions)
                    {
                        f.setVariables(1, 1, 0, 0);
                    }

                    for(Function f : graph.compFunctions)
                    {
                        f.setVariables();
                    }

                    graph.screen.updateLabel();
                }

                @Override
                public void update(Graph graph)
                {
                    if(MathUtils.ceil(graph.graphSquare.x) >= graph.x)
                    {
                        graph.graphSquare.setX(graph.x);
                        graph.updatePosition();
                        graph.stateMachine.changeState(IDLE);
                    }
                    else
                    {
                        graph.graphSquare.setX(MathUtils.lerp(graph.graphSquare.x, graph.x, Gdx.graphics.getDeltaTime() * 5));
                        graph.updatePosition();
                    }
                }
            };

    @Override
    public void enter(Graph entity)
    {

    }

    @Override
    public void exit(Graph entity)
    {

    }

    @Override
    public boolean onMessage(Graph entity, Telegram telegram)
    {
        return false;
    }
}