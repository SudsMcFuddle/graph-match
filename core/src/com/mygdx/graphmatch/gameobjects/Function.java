package com.mygdx.graphmatch.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.Random;

/**
 * Created by Oliver on 7/23/2015.
 *
 * Functions are drawn on the graph and can be edited
 */
public class Function implements IDrawShape
{
    //Used to determine what changes the function
    public enum FunctionType
    {
        PLAYER_CONTROLLED, COMP_CONTROLLED
    }

    private FunctionType functionType;

    //Variable locations in the variable array
    public static final int MULTIPLIER = 0, X_OFFSET = 1, POWER = 2, Y_OFFSET = 3;

    //Level of detail on the function line
    static final int DETAIL = 6;

    //Graph that the function will be drawn on
    private Graph graph;

    //Lines drawn to represent the function
    protected Line[] functionLines;

    //Array of variables
    private int[] var = {0, 0, 0, 0};

    //Used to randomize function variables
    private Random random;

    //Limit the number of function lines to draw (used for transitioning)
    int drawLimit = 0;

    //Color of the function line
    private Color color;

    public Function(Graph g, Color c, FunctionType type)
    {
        graph = g;
        color = c;
        functionType = type;

        random = new Random();

        for(int i = 0; i < var.length; i++)
        {
            var[i] = random.nextInt(10) - 5;
        }

        //Instantiate the line array. Size of the array is determined by graph size and level of detail
        functionLines = new Line[graph.size * DETAIL];

        //Represents the x coordinate on the left side of the graph, based on size of said graph
        float functionX = -(graph.size/2);

        //Actual x and y positions of the function on the screen
        float x = graph.x;

        //Iterate through each line in the array
        for(int i = 0; i < functionLines.length; i++)
        {
            //Instantiate the line. Set the start and end of the line to function coordinates
            functionLines[i] = new Line(x, getFunctionY(functionX), x + graph.width/functionLines.length, getFunctionY(functionX + 1f/DETAIL));

            //Increment the x and y positions and the x coordinate
            x += graph.width/functionLines.length;
            functionX += 1f/DETAIL;
        }
    }

    public Function(Graph g, Color c, FunctionType type, int m, int p, int xOff, int yOff)
    {
        graph = g;

        color = c;

        //Set each int in the array
        var[MULTIPLIER] = m;
        var[POWER] = p;
        var[X_OFFSET] = xOff;
        var[Y_OFFSET] = yOff;

        functionType = type;

        random = new Random();

        //Instantiate the line array. Size of the array is determined by graph size and level of detail
        functionLines = new Line[graph.size * DETAIL];

        //Represents the x coordinate on the left side of the graph, based on size of said graph
        float functionX = -(graph.size/2);

        //Actual x and y positions of the function on the screen
        float x = graph.x;

        //Iterate through each line in the array
        for(int i = 0; i < functionLines.length; i++)
        {
            //Instantiate the line. Set the start and end of the line to function coordinates
            functionLines[i] = new Line(x, getFunctionY(functionX), x + graph.width/functionLines.length, getFunctionY(functionX + 1f/DETAIL));

            //Increment the x and y positions and the x coordinate
            x += graph.width/functionLines.length;
            functionX += 1f/DETAIL;
        }
    }

    {

    }

    public FunctionType getFunctionType()
    {
        return functionType;
    }

    //Update the function line. Use this whenever any of the variables are changed
    private void updateFunction()
    {
        float functionX = -(graph.size/2);
        float x = graph.x;

        for(Line l : functionLines)
        {
            l.setStartPoint(x, getFunctionY(functionX));
            l.setEndPoint(x + graph.width/functionLines.length, getFunctionY(functionX + 1f/DETAIL));

            x += graph.width/functionLines.length;
            functionX += 1f/DETAIL;
        }
    }

    public int getVariable(int varLoc)
    {
        return var[varLoc];
    }

    //Sets each variable to the given parameters
    public void setVariables(int m, int p, int xOff, int yOff)
    {
        var[MULTIPLIER] = m;
        var[POWER] = p;
        var[X_OFFSET] = xOff;
        var[Y_OFFSET] = yOff;
        updateFunction();
        updatePosition();
    }

    //Sets each variable to a random integer
    public void setVariables()
    {
        for(int i = 0; i < var.length; i++)
        {
            var[i] = random.nextInt(10) - 5;
        }

        updateFunction();
        updatePosition();
    }

    //Increment the given variable
    public void increment(int varLoc)
    {
        var[varLoc]++;
        updateFunction();
    }

    //Decrement the given variable
    public void decrement(int varLoc)
    {
        var[varLoc]--;
        updateFunction();
    }

    //Returns the Y position for the given X position based on the function variables
    float getFunctionY(float functionX)
    {
        return graph.y + graph.height/2 +
        var[MULTIPLIER] * (float)Math.pow(functionX + var[X_OFFSET], var[POWER]) * (graph.height/functionLines.length*DETAIL) +
        (var[Y_OFFSET] * graph.height/functionLines.length*DETAIL);
    }

    //Used for drawing the function at the beginning of the game
    void addLine()
    {
        drawLimit++;
    }

    //Change the function's X position based on the graph's position
    void updatePosition()
    {
        float x = graph.graphSquare.x;

        for(int i = 0; i < functionLines.length; i++)
        {
            //Instantiate the line. Set the start and end of the line to function coordinates
            functionLines[i].setStartX(x);
            functionLines[i].setEndX(x + graph.width/functionLines.length);

            //Increment the x and y positions and the x coordinate
            x += graph.width/functionLines.length;
        }
    }

    //Determine if the two functions are the same by checking if the start and end points are the same.
    public boolean matches(Function f)
    {
        if(functionLines[0].getStartPoint().equals(f.functionLines[0].getStartPoint()) &&
        functionLines[functionLines.length-1].getEndPoint().equals(f.functionLines[functionLines.length-1].getEndPoint())) return true;
        else return false;
    }

    @Override
    public void draw(ShapeRenderer shape)
    {
        //Iterate through and draw each function line
        shape.begin(ShapeRenderer.ShapeType.Line);
        shape.setColor(color);
        Gdx.gl.glLineWidth(3);
        for(int i = 0; i < drawLimit; i++)
        {
            shape.line(functionLines[i].getStartX(), functionLines[i].getStartY(), functionLines[i].getEndX(), functionLines[i].getEndY());
        }
        shape.end();
    }

    @Override
    public void setColor(Color c)
    {
        color = c;
    }

    @Override
    public void setLineThickness(int thickness)
    {

    }
}
