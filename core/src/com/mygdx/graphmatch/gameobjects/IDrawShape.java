package com.mygdx.graphmatch.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by Oliver on 7/21/2015.
 *
 * Interface for any object that draws using ShapeRenderer
 */
public interface IDrawShape
{
    void draw(ShapeRenderer shape);

    void setColor(Color color);

    void setLineThickness(int thickness);
}
