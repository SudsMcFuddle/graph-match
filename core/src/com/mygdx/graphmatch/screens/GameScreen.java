package com.mygdx.graphmatch.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.graphmatch.GraphMatch;

/**
 * Created by Oliver on 7/21/2015.
 *
 * Base game screen class. The screens will control game logic and rendering
 */
public abstract class GameScreen implements Screen
{
    //Main camera and viewport
    protected OrthographicCamera camera;
    protected ScreenViewport viewport;

    protected TextureAtlas atlas;
    protected Skin skin;

    protected Stage stage;
    protected Table table;

    public GameScreen()
    {
        //Instantiates the camera and viewport
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        viewport = new ScreenViewport(camera);

        atlas = new TextureAtlas(Gdx.files.internal("buttons/buttons.atlas"));
        skin = new Skin(Gdx.files.internal("skin.json"), atlas);

        stage = new Stage(viewport, GraphMatch.spriteBatch);
        table = new Table();
    }

    @Override
    public void show()
    {
        table.setFillParent(true);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta)
    {
        //Update the camera
        camera.update();

        //Projects the sprite batch and shape renderer to the screen's camera
        GraphMatch.spriteBatch.setProjectionMatrix(camera.combined);
        GraphMatch.shapeRenderer.setProjectionMatrix(camera.combined);

        //Render everything on the screen
        //GraphMatch.spriteBatch.begin();
        draw(GraphMatch.spriteBatch, GraphMatch.shapeRenderer);
        //GraphMatch.shapeRenderer.end();

        stage.act(delta);
        stage.draw();
    }

    public abstract void draw(SpriteBatch batch, ShapeRenderer shape);

    @Override
    public void resize(int width, int height)
    {
        //Update the screen's viewport
        viewport.update(width, height);
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }
}
