package com.mygdx.graphmatch.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.mygdx.graphmatch.gameobjects.Function;
import com.mygdx.graphmatch.gameobjects.Graph;
import com.mygdx.graphmatch.gameobjects.GraphState;

/**
 * Created by Oliver on 7/21/2015.
 */
public class GameplayScreen extends GameScreen
{
    //Graph to draw on the screen
    private Graph graph;

    //Buttons for editing the player's function
    private Button[] upButtons;
    private Button[] downButtons;

    //Labels to display the function (and their strings)
    private Label[] labels;
    private String[] labelStrings;

    //Function drawn on the graph
    private Function function;
    private Function function2;

    public GameplayScreen()
    {
        super();

        //Instantiate the graph
        graph = new Graph(this, Color.BLACK, 12, Gdx.graphics.getWidth()/2 - 150, Gdx.graphics.getHeight()/2 - 150, 300, 300);

        //Instantiate function and add it to the graph
        function = new Function(graph, Color.BLUE, Function.FunctionType.PLAYER_CONTROLLED, 1, 1, 0, 0);
        function2 = new Function(graph, Color.RED, Function.FunctionType.COMP_CONTROLLED, -1, 1, 0, -3);
        graph.addFunction(function);
        graph.addFunction(function2);

        labelStrings = new String[] {"(x + ", ")^", " + ", ""};
    }

    @Override
    public void show()
    {
        //Up and down button styles
        Button.ButtonStyle up = skin.get("up_button", Button.ButtonStyle.class);
        Button.ButtonStyle down = skin.get("down_button", Button.ButtonStyle.class);

        //Instantiate up and down button arrays
        upButtons = new Button[4];
        downButtons = new Button[4];

        //Instantiate each individual button and add listeners
        for(int i = 0; i < upButtons.length; i++)
        {
            upButtons[i] = new Button(up);
            upButtons[i].addListener(new UpButtonListener(i));
            downButtons[i] = new Button(down);
            downButtons[i].addListener(new DownButtonListener(i));
        }

        //Instantiate labels
        labels = new Label[4];

        //Move the screen's table to the bottom of the screen
        table.bottom();

        //Add the up buttons to the top of the table
        for(Button b : upButtons)
        {
            table.add(b).left().pad(5);
        }
        table.row().height(50);

        //Add the labels under the up buttons
        for(int i = 0; i < labels.length; i++)
        {
            labels[i] = new Label(function.getVariable(i) + labelStrings[i], skin);
            table.add(labels[i]);
        }
        table.row();

        //Add the down buttons under the labels
        for(Button b : downButtons)
        {
            table.add(b).left().pad(5);
        }
        super.show();
    }

    @Override
    public void draw(SpriteBatch batch, ShapeRenderer shape)
    {
        //Draw the graph
        graph.draw(shape);
    }

    //Change the labels to reflect changes in the function
    public void updateLabel()
    {
        for(int i = 0; i < labels.length; i++)
        {
            labels[i].setText(function.getVariable(i) + labelStrings[i]);
        }
    }

    //Inner classes for each buttons, up and down.
    //The buttons increment and decrement the player function variables. If the functions match, move the graph off screen
    private class DownButtonListener extends InputListener
    {
        private int variable;

        private DownButtonListener(int v)
        {
            variable = v;
        }

        @Override
        public boolean touchDown(InputEvent e, float x, float y, int pointer, int button)
        {
            if (graph.getState() == GraphState.IDLE)
            {
                function.decrement(variable);
                updateLabel();
                if(function.matches(function2)) graph.setState(GraphState.MOVE_OFF_SCREEN);
                return true;
            }
            else return false;
        }
    }

    private class UpButtonListener extends InputListener
    {
        private int variable;

        private UpButtonListener(int v)
        {
            variable = v;
        }

        @Override
        public boolean touchDown(InputEvent e, float x, float y, int pointer, int button)
        {
            if (graph.getState() == GraphState.IDLE)
            {
                function.increment(variable);
                updateLabel();
                if(function.matches(function2)) graph.setState(GraphState.MOVE_OFF_SCREEN);
                return true;
            }
            else return false;
        }
    }
}
